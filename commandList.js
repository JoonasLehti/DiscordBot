// list of commands and their features/requirements specified
export const commands = [
  {
    name: 'ping',
    description: 'Replies with Dong!',
  },
  {
    name: 'random',
    description: 'Get random game (Due to API limitations only returns ~40 games)',
  },
  {
    name: 'games',
    description: 'Get game of a specific name',
    options: [
      {
        name: 'name',
        type: 3,
        description: 'Name of the game',
        required: true,
      },
    ],
  },
];