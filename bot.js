import { config } from 'dotenv';
import { InteractionResponse, REST, Routes } from 'discord.js';
import { Client, GatewayIntentBits } from 'discord.js';
import * as botCommands from './botCommands.js';
import { commands as commandList } from "./commandList.js"

// Load environment variables
config();

const client = new Client({ intents: [GatewayIntentBits.Guilds] });

const commands = commandList;

const TOKEN = process.env.CLIENT_TOKEN; 
const CLIENT_ID = process.env.ID;

const rest = new REST({ version: '10' }).setToken(TOKEN);

try {
  console.log('Started refreshing application (/) commands.');

  await rest.put(Routes.applicationCommands(CLIENT_ID), { body: commands });
  console.log('Successfully reloaded application (/) commands.');
} catch (error) { console.error(error); }

client.on('ready', () => {console.log(`Logged in as ${client.user.tag}!`);});

client.on('interactionCreate', async interaction => {
  if (!interaction.isChatInputCommand()) return;
  // Test command
  if (interaction.commandName === 'ping') {
    await interaction.reply('Pong!');
  }
  // Get info about a game from CoOptimus website using /games "name" on discord
  if (interaction.commandName === 'games') {
    try {
      botCommands.getGameInfo(interaction);
    } catch (error) { console.error(error); }
  }
  if (interaction.commandName === 'random') {
    try {
      botCommands.getRandomGame(interaction);
    } catch (error) { console.error(error); }
  }
});

client.login(TOKEN);