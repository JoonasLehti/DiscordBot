import axios from "axios";
import xml2js from 'xml2js';

// Get game info from CoOptimus site
export async function getGameInfo(interaction) {
  const gameName = interaction.options.getString('name');
  let response = await axios.get(`https://api.co-optimus.com/games.php?search=true&name=${encodeURIComponent(gameName)}&systemName=PC`);
  xml2js.parseString(response.data, async (err, result) => {
    if (err) {
      console.error(err);
      return;
    }
    try {
      if (!result.games.game || result.games.game.length === 0) {
        await interaction.reply(`No games found with the name "${gameName}".`);
        return;
      }
      const title = result.games.game[0].title[0];
      const publisher = result.games.game[0].publisher[0];
      const release = result.games.game[0].releasedate[0];
      const features = result.games.game[0].featurelist;
      const experience = result.games.game[0].coopexp[0]
      await interaction.reply(`${title} made by ${publisher} released in ${release}\nFeatures: ${features}\n\n${experience}`);
    } catch (error) { console.error(error); }
  });
}

// Get random game info from CoOptimus site
export async function getRandomGame(interaction) {
  let response = await axios.get(`https://api.co-optimus.com/games.php?search=true&systemName=PC&spc=Y`);
  let fixedResponse = response.data.replace(/&/g, "&amp;");
  xml2js.parseString(fixedResponse, async (err, result) => {
    if (err) {
      console.error(err);
      return;
    }
    try {
      if (result.games.game.length === 0) {
        await interaction.reply(`No games found. The website might be having issues.`);
        return;
      }
    console.log(result.games.game.length);
    const index = Math.floor(Math.random() * result.games.game.length);
    const title = result.games.game[index].title[0];
    const publisher = result.games.game[index].publisher[0];
    const release = result.games.game[index].releasedate[0];
    const features = result.games.game[index].featurelist;
    const experience = result.games.game[index].coopexp[0]
    await interaction.reply(`${title} made by ${publisher} released in ${release}\nFeatures: ${features}\n\n${experience}`);
  } catch (error) { console.error(error); }
  });
}